import {ModuleWithProviders} from  '@angular/core';
import {Routes, RouterModule} from '@angular/router';

import {LoginComponent} from './login/login.component';
import {DashboardComponent} from './dashboard/dashboard.component' 
import {AttendanceComponent} from './attendance/attendance.component' 
import {DetailsComponent} from './details/details.component' 

export const router: Routes=[
	{path:'dashboard',
    component: DashboardComponent,
		children: [
          {
            path: 'attendance',
            component: AttendanceComponent
          },
          {
            path: '',
            component: DetailsComponent
          }
        ]}
	,
	{path:'', component: LoginComponent}
]
export const routes: ModuleWithProviders=RouterModule.forRoot(router);