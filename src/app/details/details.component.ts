import { Component, OnInit } from '@angular/core';
import {GetUserDetailsService} from '../services/get-user-details.service';
@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.css']
})
export class DetailsComponent implements OnInit {
  userName:any;
  constructor( private eventService :GetUserDetailsService ) { }

  ngOnInit() {
    this.userName=this.eventService.sendData();
		console.log(this.userName)
  }

}
