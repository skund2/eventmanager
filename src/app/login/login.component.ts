import { Component, NgZone , OnInit } from '@angular/core';
import { ElectronService } from 'ngx-electron';
import {Router} from '@angular/router';
import { Injectable } from '@angular/core';
//declare var electron:any;
const remote = (<any> window).electron.remote;
const BrowserWindow=remote.BrowserWindow;
const ipcRenderer=(<any> window).electron.ipcRenderer;
import {GetUserDetailsService} from '../services/get-user-details.service'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
@Injectable()
export class LoginComponent implements OnInit {
	event={
		id:1,
		name:'Event name',
		img:'../src/assets/images/SN_Stamp.png',
		loginDetails:'Type of our collaborative culture cel-ebrates breakthrough ideas',
		showSpinner:true
	}

	accesstoken:any;
	constructor(private _electronService: ElectronService, private _ngZone: NgZone, private router: Router,private eventService :GetUserDetailsService) { 
		this._electronService.ipcRenderer.on('authWindow-closed', (event, arg) => {
            this._ngZone.run(() => {
                this.event.showSpinner=true;
            });
        })
	}


	ngOnInit() {
	}
	loginBySSO() {
	    console.log('lala');
	    ipcRenderer.send('do-login');
	    this.event.showSpinner=false;
	    ipcRenderer.on('asynchronous-reply', (event, arg) => {
		 console.log(arg);
		 this.accesstoken= this.eventService.sendAccessToken(arg).then(arg=>{
		 	this.router.navigate(['dashboard'])
		 });
		}) 
	}

}
