import { Injectable } from '@angular/core';
import {Http, Headers, Response} from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/toPromise';
const remote = (<any> window).electron.remote;
const session=remote.session;

@Injectable()
export class GetUserDetailsService {
 token:Observable<any>;
 userData:any;
 test:any;
  constructor(private http: Http){}
  ngOnInit() {	

  }
  sendAccessToken(token){
  	console.log(token);
	var authHeader = new Headers();
	var parseJiveResponse = function(response) {
	    response=response._body;
	    var trimmedResponse = response.replace('throw \'allowIllegalResourceCall is false.\';', '');
	    return trimmedResponse !== '' ? JSON.parse(trimmedResponse) : {};
	};
	authHeader.append('Authorization', 'Bearer ' +token);
	return this.http.get('https://vox-uat.publicis.sapient.com/api/core/v3/people/@me', {
	    headers: authHeader
	  }).map((res:Response) => {
		  	if (res.status==200) {
		  		var userDetail = parseJiveResponse(res);
		  		var email;
	            for (var i in userDetail.emails) {
	                if (userDetail.emails[i].type == 'work') {
	                    email = userDetail.emails[i].value;
	                    break;
	                }
	            }
	            var profile = {};
	            for (var j in userDetail.jive.profile) {
	                profile[userDetail.jive.profile[j]['jive_label']] = userDetail.jive.profile[j]['value'];
	            }
	            var contactNumber = {};
	            for (var k in userDetail.phoneNumbers) {
	                contactNumber[userDetail.phoneNumbers[k]['jive_label']] = userDetail.phoneNumbers[k]['value'];
	            }
	            var userInfo = {
	                name: profile['Display Name'],
	                email: email,
	                careerStage: profile['Career Stage'],
	                profilePic: userDetail.thumbnailUrl,
	                ntId: userDetail.jive.username,
	                homeLocation: profile['Home Office'],
	                contact: contactNumber['Mobile Phone'],
	                isActive: 1
	            };
	            console.log(userInfo);
	            this.userData=userInfo;
	            return userInfo;
	            
		  	}  		
		}).toPromise()


    }
	sendData(){
		console.log(this.userData+ 'this.userData');
			var user=this.userData;
			return user
	}


}
