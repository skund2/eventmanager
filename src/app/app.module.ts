import { BrowserModule } from '@angular/platform-browser';
import { NgModule} from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { MaterialModule } from '@angular/material';
import {NgxElectronModule} from 'ngx-electron';

import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import {GetUserDetailsService} from './services/get-user-details.service'

import 'hammerjs'
import {routes} from './app.router';
import { AttendanceComponent } from './attendance/attendance.component';
import { NotificationComponent } from './notification/notification.component';
import { DetailsComponent } from './details/details.component'
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    DashboardComponent,
    DashboardComponent,
    AttendanceComponent,
    NotificationComponent,
    DetailsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routes,
    NgxElectronModule,
    MaterialModule.forRoot()
  ],
  providers: [GetUserDetailsService],
  bootstrap: [AppComponent]
})
export class AppModule { }
