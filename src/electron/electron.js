const electron = require('electron');
const electronOauth2 = require('electron-oauth2');
const Promise = require('pinkie-promise');
const queryString = require('querystring');
const fetch = require('node-fetch');
const objectAssign = require('object-assign');
const nodeUrl = require('url');
const {app, BrowserWindow}=electron;
const {session} = require('electron');
const ipc = electron.ipcMain;
require('electron-reload')(__dirname);
let loginWindow =null;
var config = {
    clientId: 'YOUR_CLIENTID',
    clientSecret: 'YOUR_CLIENT_SECRET',
    authorizationUrl: 'YOUR_Authorization_Url',
    tokenUrl: 'Your_Token_Url',
    useBasicAuthorizationHeader: false,
    redirectUri: 'http://localhost:80/callback'
};
app.on('ready',_ => {
  loginWindow = new BrowserWindow({
    width:1200,
    height:725, 
    resizable: false
  });
  loginWindow.loadURL(`file://${__dirname}/index.html`);
  loginWindow.webContents.openDevTools();
  var useJive = function() {
      
  };
  loginWindow.on('closed', _=>{
    loginWindow=null
  });
  ipc.on('do-login', (req, res) => {
        /*loginWindow.show();*/
      const windowParams = {
        alwaysOnTop: true,
        autoHideMenuBar: true,
        webPreferences: {
            nodeIntegration: false
        }
      }
      /*authWindow.on('close', (e) => {
        console.log('dada');
      });*/
           
      const options = {
        scope: 'SCOPE',
        accessType: 'ACCESS_TYPE'
      };
           
      function myApiOauth (config, windowParams) {
      function getAuthorizationCode(opts) {
        opts = opts || {};

        if (!config.redirectUri) {
          config.redirectUri = 'urn:ietf:wg:oauth:2.0:oob';
        }

        var urlParams = {
          response_type: 'code',
          redirect_uri: config.redirectUri,
          client_id: config.clientId
        };

        if (opts.scope) {
          urlParams.scope = opts.scope;
        }

        if (opts.accessType) {
          urlParams.access_type = opts.accessType;
        }

        var url = config.authorizationUrl + '?' + queryString.stringify(urlParams);

        return new Promise(function (resolve, reject) {
          const authWindow = new BrowserWindow(windowParams || {'use-content-size': true});

          authWindow.loadURL(url);
          authWindow.show();

          authWindow.on('closed', () => {
            req.sender.send('authWindow-closed', 'closed');
            reject(new Error('window was closed by user'));
            console.log('just closed');
          });

          function onCallback(url) {
            var url_parts = nodeUrl.parse(url, true);
            var query = url_parts.query;
            var code = query.code;
            var error = query.error;

            if (error !== undefined) {
              reject(error);
              authWindow.removeAllListeners('closed');
              setImmediate(function () {
                authWindow.close();
              });
            } else if (code) {
              resolve(code);
              authWindow.removeAllListeners('closed');
              setImmediate(function () {
                authWindow.close();
              });
            }
          }

          authWindow.webContents.on('will-navigate', (event, url) => {
            onCallback(url);
          });

          authWindow.webContents.on('did-get-redirect-request', (event, oldUrl, newUrl) => {
            onCallback(newUrl);
          });
        });
      }

      function tokenRequest(data) {
        const header = {
          'Accept': 'application/json',
          'Content-Type': 'application/x-www-form-urlencoded'
        };

        if (config.useBasicAuthorizationHeader) {
          header.Authorization = 'Basic ' + new Buffer(config.clientId + ':' + config.clientSecret).toString('base64');
        } else {
          objectAssign(data, {
            client_id: config.clientId,
            client_secret: config.clientSecret
          });
        }

        return fetch(config.tokenUrl, {
          method: 'POST',
          headers: header,
          body: queryString.stringify(data)
        }).then(res => {
          return res.json();
        });
      }

      function getAccessToken(opts) {
        return getAuthorizationCode(opts)
          .then(authorizationCode => {
            var tokenRequestData = {
              code: authorizationCode,
              grant_type: 'authorization_code',
              redirect_uri: config.redirectUri
            };
            tokenRequestData = Object.assign(tokenRequestData, opts.additionalTokenRequestData);
            return tokenRequest(tokenRequestData);
          });
      }

      function refreshToken(refreshToken) {
        return tokenRequest({
          refresh_token: refreshToken,
          grant_type: 'refresh_token',
          redirect_uri: config.redirectUri
        });
      }
      getAccessToken(options).then(token => {
        console.log(token.access_token);

        const cookie = {url: 'https://eventdesktop.com', name: 'access_token', value:token.access_token}
        session.defaultSession.cookies.set(cookie, (error) => {
          if (error) console.error(error)
        })
        if (token) {
            console.log('inside token');
            req.sender.send('asynchronous-reply', token.access_token);    
        }  
      });
      };
      myApiOauth(config, windowParams);
      console.log('it worked');
  })
})